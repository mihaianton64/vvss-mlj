package inventory.inventory;

public interface IHasID<ID> {
    ID getId();
    void setId(ID id);
}
