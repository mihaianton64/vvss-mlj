package inventory.repository;


import inventory.inventory.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.StringTokenizer;

public class InventoryRepository {

//    private static String filename = "data/items.txt";
    private Inventory inventory;
    private static String filename = "data.txt";

    public InventoryRepository(String filename) {
        try {
            File file = new File("data.txt");
            file.createNewFile();
            System.out.println("File: " + file);
        } catch(Exception e) {
            e.printStackTrace();
        }

        this.inventory = new Inventory();
        readParts();
        readProducts();
    }

    public void readParts() {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File("data.txt");
        ObservableList<Part> listP = FXCollections.observableArrayList();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) != null) {
                Part part = getPartFromString(line);
                if (part != null)
                    listP.add(part);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        inventory.setAllParts(listP);
    }

    private Part getPartFromString(String line) {
        Part item = null;
        if (line == null || line.equals("")) return null;
        StringTokenizer st = new StringTokenizer(line, ",");
        String type = st.nextToken();
        if (type.equals("I")) {
            int id = Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name = st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            int idMachine = Integer.parseInt(st.nextToken());
            item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
        }
        if (type.equals("O")) {
            int id = Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name = st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String company = st.nextToken();
            item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
        }
        return item;
    }

    public void readProducts() {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File("data.txt");

        ObservableList<Product> listP = FXCollections.observableArrayList();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) !=  null ) {
                Product product = getProductFromString(line);
                if (product != null)
                    listP.add(product);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        inventory.setProducts(listP);
    }

    private Product getProductFromString(String line) {
        System.out.println(line);
        Product product = null;
        if (line == null || line.equals("")) return null;
        StringTokenizer st = new StringTokenizer(line, ",");
        String type = st.nextToken();
        if (type.equals("P")) {
            int id = Integer.parseInt(st.nextToken());
            inventory.setAutoProductId(id);
            String name = st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String partIDs = st.nextToken();

            StringTokenizer ids = new StringTokenizer(partIDs, ":");
            ObservableList<Part> list = FXCollections.observableArrayList();
            while (ids.hasMoreTokens()) {
                String idP = ids.nextToken();
                Part part = inventory.lookupPart(idP);
                if (part != null)
                    list.add(part);
            }
            product = new Product(id, name, price, inStock, minStock, maxStock, list);
            product.setAssociatedParts(list);
        }
        return product;
    }

    public void writeAll() throws IOException {

        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File("data.txt");

        BufferedWriter bw = null;
        ObservableList<Part> parts = inventory.getAllParts();
        ObservableList<Product> products = inventory.getProducts();

        try {
            bw = new BufferedWriter(new FileWriter(file));
            for (Part p : parts) {
                System.out.println(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }

            for (Product pr : products) {
                String line = pr.toString() + ",";
                ObservableList<Part> list = pr.getAssociatedParts();
                int index = 0;
                while (index < list.size() - 1) {
                    line = line + list.get(index).getId() + ":";
                    index++;
                }
                if (index == list.size() - 1)
                    line = line + list.get(index).getId();
                bw.write(line);
                bw.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bw.close();
        }
    }

    public void addPart(Part part) throws IOException {
        inventory.addPart(part);
        writeAll();
    }

    public void addProduct(Product product) throws IOException {
        inventory.addProduct(product);
        writeAll();
    }

    public int getAutoPartId() {
        return inventory.getAutoPartId();
    }

    public int getAutoProductId() {
        return inventory.getAutoProductId();
    }

    public ObservableList<Part> getAllParts() {
        return inventory.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return inventory.getProducts();
    }

    public Part lookupPart(String search) {
        return inventory.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return inventory.lookupProduct(search);
    }

    public void updatePart(int partIndex, Part part) throws IOException {
        inventory.updatePart(partIndex, part);
        writeAll();
    }

    public void updateProduct(int productIndex, Product product) throws IOException {
        inventory.updateProduct(productIndex, product);
        writeAll();
    }

    public void deletePart(Part part) throws IOException {
        inventory.deletePart(part);
        writeAll();
    }

    public void deleteProduct(Product product) throws IOException {
        inventory.removeProduct(product);
        writeAll();
    }

    public Inventory getInventory() {
        Inventory newInventory = new Inventory();
        ObservableList<Part> newParts = FXCollections.observableArrayList();
        ObservableList<Product> newProducts = FXCollections.observableArrayList();
        newParts.addAll(inventory.getAllParts());
        newProducts.addAll(inventory.getProducts());
        newInventory.setAllParts(newParts);
        newInventory.setProducts(newProducts);
        return newInventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }
}
