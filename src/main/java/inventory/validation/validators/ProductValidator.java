package inventory.validation.validators;

import inventory.inventory.Part;
import inventory.inventory.Product;
import inventory.validation.exceptions.ProductValidationException;

public class ProductValidator implements IValidator<Product> {

    /**
     * Function to validate a Product object
     * @param p: Product instance to be validated
     * @throws ProductValidationException : throws ProductValidationException if the Product object does not meets
     * the requirements
     */
    @Override
    public void validate(Product p) throws ProductValidationException {
        double sumOfParts = p.getAssociatedParts().stream().mapToDouble(Part::getPrice).sum();
        String errorMessage = "";
        if(p.getName().equals("")){
            errorMessage += "A name has not been entered. ";
        }
        if (p.getMin() < 0) {
            errorMessage += "The inventory level must be greater than 0. ";
        }
        if (p.getPrice() < 0.01) {
            errorMessage += "The price must be greater than $0. ";
        }
        if (p.getMin() > p.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(p.getInStock() < p.getMin()) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(p.getInStock() > p.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        if (p.getAssociatedParts().size() < 1) {
            errorMessage += "Product must contain at least 1 part. ";
        }
        if (sumOfParts > p.getPrice()) {
            errorMessage += "Product price must be greater than cost of parts. ";
        }

        if(errorMessage.length() > 0){
            throw new ProductValidationException(errorMessage);
        }
    }
}
