package inventory.validation.validators;

public interface IValidator<T> {
    void validate(T t) throws Exception;
}
