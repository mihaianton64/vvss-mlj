package inventory.validation.validators;

import inventory.inventory.Part;
import inventory.validation.exceptions.PartValidationException;

public class PartValidator implements IValidator<Part> {

    /**
     * Function to validate a Part object
     * @param p: Part instance to be validated
     * @throws PartValidationException: throws PartValidationException if the Part object does not meets
     * the requirements
     */
    @Override
    public void validate(Part p) throws PartValidationException {
        String errorMessage = "";
        if(p.getName().equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if(p.getPrice() < 0.01) {
            errorMessage += "The price must be greater than 0. ";
        }
        if(p.getInStock() < 1) {
            errorMessage += "Inventory level must be greater than 0. ";
        }
        if(p.getMin() > p.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(p.getInStock() < p.getMin()) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(p.getInStock() > p.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }

        if(errorMessage.length() > 0){
            throw new PartValidationException(errorMessage);
        }
    }
}
