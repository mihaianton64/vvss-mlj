package inventory.validation.exceptions;

public class ProductValidationException extends GenericException {
    /**
     * Constructor for ProductValidationException
     * @param message: error message
     */
    public ProductValidationException(String message) {
        super(message);
    }
}
