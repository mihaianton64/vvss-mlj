package inventory.validation.exceptions;

public class PartValidationException extends GenericException {
    /**
     * Constructor for PartValidationException
     * @param message: error message
     */
    public PartValidationException(String message) {
        super(message);
    }
}
