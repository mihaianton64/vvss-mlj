package inventory.validation.exceptions;

public abstract class GenericException extends RuntimeException {
    /**
     * Constructor for GenericException class
     * @param message: error message
     */
    public GenericException(String message){
        super(message);
    }
}
