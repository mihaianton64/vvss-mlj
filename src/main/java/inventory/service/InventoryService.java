package inventory.service;

import inventory.inventory.*;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;

import java.io.IOException;

public class InventoryService {

    private InventoryRepository repo;

    public InventoryService(InventoryRepository repo){
        this.repo =repo;
    }


    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        try {
            repo.addPart(inhousePart);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue) {
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        try {
            repo.addPart(outsourcedPart);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts){
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        try {
            repo.addProduct(product);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    public void updateInhousePart(int partIndex, int partId, String name, double price, int inStock, int min, int max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(partId, name, price, inStock, min, max, partDynamicValue);
        try {
            repo.updatePart(partIndex, inhousePart);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(partId, name, price, inStock, min, max, partDynamicValue);
        try {
            repo.updatePart(partIndex, outsourcedPart);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts){
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        try {
            repo.updateProduct(productIndex, product);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deletePart(Part part){
        try {
            repo.deletePart(part);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteProduct(Product product){
        try {
            repo.deleteProduct(product);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
