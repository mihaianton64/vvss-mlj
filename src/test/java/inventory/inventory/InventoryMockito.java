package inventory.inventory;

import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("InvMockito")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InventoryMockito {

    private static Inventory inv1;
    private static Inventory inv2;

    static private InventoryRepository repo;
    static private InventoryService service;
    static private InhousePart part;
    static private ObservableList<Part> addParts;


    @BeforeEach
    public void setUp(){
        part = new InhousePart(1, "x", 0, 10, 1, 20, 10);
        addParts = FXCollections.observableArrayList();
        addParts.add(part);

        inv1 = new Inventory();
        inv2 = new Inventory();
    }

    @Test
    public void testToString() throws ParseException {
        Product p = Mockito.mock(Product.class);
        Mockito.when(p.toString()).thenReturn("Product");
        inv1.addProduct(p);
        System.out.println(inv1.getProducts().toString());

        assert inv1.getProducts().toString().equals("[Product]");
    }
    @Test
    public void testEquals() {
        Product p = Mockito.mock(Product.class);
        Product p1 = Mockito.mock(Product.class);

        Mockito.doReturn(null).when(p).equals(p);

        inv1.addProduct(p);
        inv2.addProduct(p);

        assert inv1.getProducts().toString().equals(inv2.getProducts().toString());


        inv2.removeProduct(p);
        inv2.addProduct(p1);
        assert !inv1.getProducts().toString().equals(inv2.getProducts().toString());
    }
}