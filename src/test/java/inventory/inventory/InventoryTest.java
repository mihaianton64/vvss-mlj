package inventory.inventory;

import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {

    static private InventoryRepository repo;
    static private InventoryService service;
    static private InhousePart part;
    static private ObservableList<Part> addParts;

    @BeforeAll
    static void initalSetUp(){
        part = new InhousePart(1, "x", 0, 10, 1, 20, 10);
        addParts = FXCollections.observableArrayList();
        addParts.add(part);
    }

    @BeforeEach
    void setUp() {

        repo = new InventoryRepository("src/test/java/inventory/data.txt");
        service = new InventoryService(repo);

    }

    @AfterEach
    void tearDown() {
        if(service == null){
            return;
        }
        ObservableList<Product> l = service.getAllProducts();
        for(Product p : l){
            service.deleteProduct(p);
            break;
        };
    }


    ///////////////////////////////////////// tests for lab3

    @Test
    void lookupPart() {


    }


    @Test
    void lookupProduct01() {
        try {
            service.addProduct("screw", 12.0, 2, 1, 10, addParts);

            assertNotNull(this.repo.lookupProduct("screw"));
        }
        catch (Exception err){
            fail();
        }
    }

    @Test
    void lookupProduct02() {
        try {

            service.addProduct("screw", 12.0, 2, 1, 10, addParts);

            assertEquals("screw",this.service.lookupProduct("1").getName());

        }
        catch (Exception err){
            fail();
        }
    }

    @Test
    void lookupProduct03() {
        try {
            service.addProduct("screw", 12.0, 2, 1, 10, addParts);
            assertNull(this.service.lookupProduct("xyz"));
        }
        catch (Exception err){
            err.printStackTrace()   ;
        }
    }

    @Test
    void lookupProduct04() {
        try {
            assertNull(this.repo.lookupProduct("xyz"));
        }
        catch (Exception err){
            System.out.println(err);
            fail();
        }
    }

    @Test
    void lookupProduct05() {
        try {
            assertNull(this.repo.lookupProduct("xyz12345678976543234567898765432"));
        }
        catch (Exception err){
            System.out.println(err);
            fail();
        }
    }
}