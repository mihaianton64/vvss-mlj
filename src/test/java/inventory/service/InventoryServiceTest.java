package inventory.service;

import inventory.inventory.InhousePart;
import inventory.inventory.Part;
import inventory.inventory.Product;
import inventory.repository.InventoryRepository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceTest {

    static private InventoryRepository repo;
    static private InventoryService service;
    static private InhousePart part;
    static private ObservableList<Part> addParts;

    @BeforeAll
    static void initalSetUp(){
        part = new InhousePart(1, "x", 0, 10, 1, 20, 10);
        addParts = FXCollections.observableArrayList();
        addParts.add(part);
    }

    @BeforeEach
    void setUp() {

        repo = new InventoryRepository("src/test/java/inventory/data.txt");
        service = new InventoryService(repo);

    }

    @AfterEach
    void tearDown() {
        if(service == null){
            return;
        }
        ObservableList<Product> l = service.getAllProducts();
        for(Product p : l){
            service.deleteProduct(p);
            break;
        };
    }

    @Test
    void addProduct() {


    }

    @Test
    void addProductEcpInvalid_1() {  //TC03_ECP
        try{
            service.addProduct("", 10, 10, 1, 20, addParts);
            fail();
        }
        catch (Exception e){
            assertTrue(true);
        }

    }

    @Test
    void addProductEcpInvalid_2() {  //TC01_ECP
        try{
            service.addProduct(null, 10, 10, 1, 20, addParts);
            fail();
        }
        catch (Exception e){

            assertTrue(true);
        }

    }

    @Test
    void addProductEcpValid_1() {    //TC05_ECP
        int prevSize = service.getAllProducts().size();
        try{
            service.addProduct("Name1", 10, 10, 1, 20, addParts);
        }
        catch (Exception e){

            fail();
        }
        int actSize = service.getAllProducts().size();
        assertEquals(prevSize+1, actSize);
    }


    @Test
    void addProductBVAInvalid_1() {  //TC03_BVA
        try{
            service.addProduct("screw", -1, 10, 1, 20, addParts);
        }
        catch (Exception e){

            assertTrue(true);
        }
    }

    @Test
    void addProductBVAInvalid_2() { //TC05_BVA
        try{
            service.addProduct("screw", 0, 10, 1, 20, addParts);
            fail();
        }
        catch (Exception e){

            assertTrue(true);
        }
    }

    @Test
    void addProductBVAValid_1() {   //TC04_BVA

        int prevSize = service.getAllProducts().size();

        try{
            service.addProduct("screw", 10, 10, 1, 20, addParts);
        }
        catch (Exception e){

            fail();
        }

        int actSize = service.getAllProducts().size();

        assertEquals(prevSize+1, actSize);

    }


    @Test
    void addProductBVAValid_2() {   //TC01_BVA

        int prevSize = service.getAllProducts().size();

        try{
            service.addProduct("s", 10, 10, 1, 20, addParts);
        }
        catch (Exception e){

            fail();
        }

        int actSize = service.getAllProducts().size();

        assertEquals(prevSize+1, actSize);

    }

    ///////////////////////////////////////// tests for lab3

    @Test
    void lookupPart() {


    }


    @Test
    void lookupProduct01() {
        try {
            service.addProduct("screw", 12.0, 2, 1, 10, addParts);

            assertNotNull(this.repo.lookupProduct("screw"));
        }
        catch (Exception err){
            fail();
        }
    }

    @Test
    void lookupProduct02() {
        try {

            service.addProduct("screw", 12.0, 2, 1, 10, addParts);

            assertEquals("screw",this.service.lookupProduct("1").getName());

        }
        catch (Exception err){
            fail();
        }
    }

    @Test
    void lookupProduct03() {
        try {
            service.addProduct("screw", 12.0, 2, 1, 10, addParts);
            assertNull(this.service.lookupProduct("xyz"));
        }
        catch (Exception err){
            err.printStackTrace()   ;
        }
    }

    @Test
    void lookupProduct04() {
        try {
            assertNull(this.repo.lookupProduct("xyz"));
        }
        catch (Exception err){
            System.out.println(err);
            fail();
        }
    }
}