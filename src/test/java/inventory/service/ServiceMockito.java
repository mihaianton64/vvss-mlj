package inventory.service;

import inventory.inventory.InhousePart;
import inventory.inventory.Inventory;
import inventory.inventory.Part;
import inventory.inventory.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
//import jdk.jfr.internal.Repository;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;


import java.io.IOException;
import java.text.ParseException;


import static org.junit.jupiter.api.Assertions.*;

@DisplayName("MockitoTaskServiceTest")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ServiceMockito {
    static private InventoryRepository repo;
    static private InventoryService service;
    static private Inventory inventory;
    static private InhousePart part;
    static private ObservableList<Part> addParts;

    @BeforeAll
    static void initalSetUp(){
        part = new InhousePart(1, "x", 0, 10, 1, 20, 10);
        addParts = FXCollections.observableArrayList();
        addParts.add(part);
    }

    @BeforeEach
    void setUp() {
        repo = Mockito.mock(InventoryRepository.class);
        service = new InventoryService(repo);
    }

    @Test
    public void testLookupProduct() throws ParseException, IOException {

        Product p1 = new Product(1, "p1",1,1,1,1,addParts);
        Product p2 = new Product(2, "p2",1,1,1,1,addParts);

        ObservableList<Product> obs = FXCollections.observableArrayList();
        obs.add(p1);
        obs.add(p2);

//        Mockito.when(repo.getAllProducts()).thenReturn(obs);
        Mockito.when(repo.lookupProduct(Mockito.anyString())).thenReturn(p1);
        Product filtered = service.lookupProduct("p1");

        assertNotNull(filtered);
    }

}